# codepipeline-chefzero-webapp
This repository contains [Chef](https://www.chef.io/) code used to define a simple Tomcat web application for use in a sample [AWS CodePipeline](https://aws.amazon.com/codepipeline/) project.

This repository should be used together with [colmmg/terraform/codepipeline-chefzero-webapp](https://gitlab.com/colmmg/terraform/codepipeline-chefzero-webapp).
