<!DOCTYPE html>
<html>
<head>
<title>Continuous Deployment with AWS CodePipeline and Chef Zero</title>
<style type="text/css">
body, html {
  height: 100%;
  margin: 0;
  font: 400 15px/1.8 "Lato", sans-serif;
  color: #777;
}

.bgimg {
  position: relative;
  opacity: 0.65;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 100%;
}

#blue.bgimg {
  background-image: url("/blue.jpg");
}

#green.bgimg {
  background-image: url("/green.jpg");
}

#blue #greencaption, #green #bluecaption {
  display: none;
}

.caption {
  background: black;
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  text-align: center;
  color: white;
}

a:link, a:visited, a:hover, a:active {
  color: wheat;
  text-decoration: none;
}
</style>
</head>
<body>
<div id="blue" class="bgimg">
  <div class="caption">
    <div id="bluecaption">Image by <a href="https://pixabay.com/users/EliasSch-3372715/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1913559">Elias Sch.</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1913559">Pixabay</a></div>
    <div id="greencaption">Image by <a href="https://pixabay.com/users/stevepb-282134/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1911392">Steve Buissinne</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1911392">Pixabay</a></div>
  </div>
</div>
</body>
</html>
