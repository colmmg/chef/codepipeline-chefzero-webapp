#!/bin/bash

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
COOKBOOK_NAME=codepipeline-chefzero-webapp
CHEF_ENVIRONMENT="prod"
CHEF_VERSION="15.6.10"

curl "https://packages.chef.io/files/stable/chef/${CHEF_VERSION}/el/8/chef-${CHEF_VERSION}-1.el7.x86_64.rpm" -o /tmp/chef.rpm
rpm -ivh /tmp/chef.rpm
rm -f /tmp/chef.rpm
rm -rf /etc/chef/bootstrap/
mkdir -p /etc/chef/bootstrap/
cd /etc/chef/bootstrap/
tar zxf $SCRIPT_DIR/../cookbooks.tar.gz
cp -r cookbooks/${COOKBOOK_NAME}/environments . 
/usr/bin/chef-client --chef-license accept -z -E ${CHEF_ENVIRONMENT} -o ${COOKBOOK_NAME}::default
