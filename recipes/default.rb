include_recipe 'java'

tomcat_install 'webapp' do
  version '8.0.36'
end

directory '/opt/tomcat_webapp/webapps/ROOT' do
  owner 'tomcat_webapp'
  group 'tomcat_webapp'
  mode '0755'
  action :create
end

cookbook_file '/opt/tomcat_webapp/webapps/ROOT/index.jsp' do
  source 'index.jsp'
  owner 'tomcat_webapp'
  group 'tomcat_webapp'
  mode '0644'
  action :create
end

cookbook_file '/opt/tomcat_webapp/webapps/ROOT/blue.jpg' do
  source 'blue.jpg'
  owner 'tomcat_webapp'
  group 'tomcat_webapp'
  mode '0644'
  action :create
end

cookbook_file '/opt/tomcat_webapp/webapps/ROOT/green.jpg' do
  source 'green.jpg'
  owner 'tomcat_webapp'
  group 'tomcat_webapp'
  mode '0644'
  action :create
end

tomcat_service 'webapp' do
  action :start
end
